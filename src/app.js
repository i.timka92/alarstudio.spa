import jquery from "jquery";
import _ from "underscore";
import MainRouter from "./js/routers/MainRouter";
import Backbone from "backbone";


class App {
    _selector;
    _routers;

    configure() {
        window.$ = window.jQuery = jquery;
        _.templateSettings = {
            interpolate: /\{\{(.+?)\}\}/g
        };

        // document.querySelectorAll('.header a').forEach((e) => {
        //     e.addEventListener('click', (event) => {
        //         event.preventDefault();
        //         Backbone.history.navigate(event.target.pathname, {trigger: true});
        //     })
        // });
    }

    routes() {
        this._routers = {};
        this._routers[MainRouter.name] = new MainRouter();
        // Backbone.history.start({pushState: true});
        Backbone.history.start();
    }

    run(selector) {
        this._selector = selector;

        this.configure();
        this.routes();
    }

    renderPage(page) {
        let $wrapper = document.querySelector(this._selector);

        $wrapper.innerHTML = '';
        $wrapper.appendChild(page.render());
    }
}

const app = new App();
export default app;