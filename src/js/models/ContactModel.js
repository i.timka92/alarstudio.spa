import Backbone from "backbone";
import ModelValidatorException from "../exceptions/ModelValidatorException";
import validatorService from "../services/ValidatorService";

export default Backbone.Model.extend({
    defaults: {
        name: '',
        phone: '',
    },

    validationException: null,

    initialize() {
        this.validationException = new ModelValidatorException();
    },

    validate(attrs) {
        this.validationException.clearAttributesErrors();

        this._validateName(attrs.name);
        this._validatePhone(attrs.phone);

        if (this.validationException.getAttributeErrors() !== null) {
            throw this.validationException;
        }
    },

    _validatePhone(value) {
        let pushError = (message) => this._pushValidationError('phone', message);

        if (validatorService.isEmpty(value))
            return pushError('Телефон не может быть пустым');

        if (validatorService.isPhone(value))
            return pushError('Формат телефона не допустим');
    },

    _validateName(value) {
        let pushError = (message) => this._pushValidationError('name', message);

        if (validatorService.isEmpty(value))
            return pushError('Имя не может быть пустым');
    },

    _pushValidationError(alias, message) {
        this.validationException.pushAttributeError(alias, new Error(message));
    }
});