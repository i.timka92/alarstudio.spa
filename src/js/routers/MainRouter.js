import Backbone from "backbone";
import app from "../../app";
import ContactPage from "../pages/PhonebookPage";

export default Backbone.Router.extend({
    routes: {
        '': 'phonebook',
        'phonebook': 'phonebook'
    },

    phonebook() {
        let page = new ContactPage();
        app.renderPage(page);
    }
})