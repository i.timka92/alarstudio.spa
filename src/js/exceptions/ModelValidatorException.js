import {isEmpty} from "underscore";

class ModelValidatorException extends Error {
    message;
    stack;
    attributes;

    constructor(message) {
        super(message);
        
        this.clearAttributesErrors();
        this.stack = (new Error()).stack;
        this.message = message;
    }

    pushAttributeError(alias, e) {
        this.attributes[alias] = e;
    }

    getAttributeErrors() {
        if (isEmpty(this.attributes)) {
            return null;
        }

        return this.attributes;
    }

    clearAttributesErrors() {
        this.attributes = {};
    }
}

export default ModelValidatorException;