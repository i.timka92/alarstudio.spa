import Backbone from "backbone";
import ContactModel from "../models/ContactModel";

const ContactCollection = Backbone.Collection.extend({
    model: ContactModel,
    sortParam: 'name',
    sortMode: 'asc',

    comparator(a, b) {
        let key = this.sortMode === 'asc' ? 1 : -1,
            aParam = a.get(this.sortParam),
            bParam = b.get(this.sortParam);

        if (aParam > bParam)
            return -1 * key;

        if (aParam < bParam)
            return key;

        return 0;
    }
})

const contactCollection = new ContactCollection();

export default contactCollection;
