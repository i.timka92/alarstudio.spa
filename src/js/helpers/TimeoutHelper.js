
class TimeoutHelper {
    async delay(milliseconds) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve();
            }, milliseconds)
        });
    }
}

const timeoutHelper = new TimeoutHelper();
export default timeoutHelper;