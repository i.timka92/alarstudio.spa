import Backbone from 'backbone';
import _ from 'underscore';
import template from 'bundle-text:../../templates/pages/PhonebookPage.html';
import ContactTableView from "../views/Phonebook/ContactTableView";
import NewContactFormView from "../views/Phonebook/NewContactFormView";

export default Backbone.View.extend({
    initialize() {
        this.template = _.template(template);
    },

    render() {
        this.el.innerHTML = this.template();

        let contactTableView = new ContactTableView();
        this.el.querySelector('.phonebook-table-wrapper')
            .appendChild(contactTableView.render());

        let newContactFormView = new NewContactFormView();
        this.el.querySelector('.phone-add-form-wrapper')
            .appendChild(newContactFormView.render());


        return this.el;
    }
});