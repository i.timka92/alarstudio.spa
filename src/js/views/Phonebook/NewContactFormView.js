import Backbone from "backbone";
import Mustache from 'mustache';

import tpl from 'bundle-text:../../../templates/data/Phonebook/NewContactFormView.html';
import ContactModel from "../../models/ContactModel";
import StateModel from "../../models/StateModel";
import contactCollection from "../../collections/ContactCollection";
import contactProvider from "../../services/ContactProvider";


export default Backbone.View.extend({
    // model: ContactModel,
    tagName: 'form',
    className: 'phone-add-form pure-form',

    events: {
        'click button[type="submit"]': 'onSave',
    },

    state: null,

    initialize() {
        this.template = ((template) => {
            return (params) => {
                return Mustache.render(template, params);
            };
        })(tpl)

        this._initState();
        this.render();

        this.listenTo(this.state, 'change', this.render);
    },

    render() {
        let data = {};
        data.__state = this.state.toJSON();
        this.el.innerHTML = this.template(data);

        return this.el;
    },

    onSave(event) {
        event.preventDefault();

        try {
            let name = this.el.querySelector('input[name="name"]').value,
                phone = this.el.querySelector('input[name="phone"]').value;

            this._updateState(name, phone, true);

            let model = new ContactModel()
            model.set({ name, phone,}, { validate: true });

            contactProvider
                .insert({
                    name,
                    phone
                })
                .then(() => {
                    contactCollection.add(model);
                    this._clearState();
                })
        } catch (e) {
            if (typeof e.getAttributeErrors !== 'function') {
                throw e;
            }
            this._processValidationException(e);
        }
    },

    _updateState(name, phone, silent) {
        this.state.set({
            name,
            phone
        }, {
            silent: !!silent
        });
    },

    _clearState(silent) {
        this.state.set({
            error: null,
            name: undefined,
            phone: undefined
        }, {
            silent: !!silent
        })
    },

    _initState() {
        this.state = new StateModel({
            error: null,
            name: undefined,
            phone: undefined
        }, {
            silent: true
        });
    },

    _processValidationException(e) {
        let error = {},
            attributeErrors = e.getAttributeErrors();

        for (let prop in attributeErrors) {
            if (!attributeErrors.hasOwnProperty(prop)) {
                continue;
            }
            error[prop] = attributeErrors[prop].message;
        }

        this.state.set({error})
    }
});