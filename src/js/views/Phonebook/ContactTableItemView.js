import Backbone from "backbone";
import Mustache from 'mustache';

import tpl from 'bundle-text:../../../templates/data/Phonebook/ContactTableItemView.html';

import ContactModel from "../../models/ContactModel";
import StateModel from "../../models/StateModel";
import contactProvider from "../../services/ContactProvider";


export default Backbone.View.extend({
    model: ContactModel,
    tagName: 'tr',

    events: {
        'click button[on-edit]': 'onEdit',
        'click button[on-delete]': 'onDelete',
        'click button[on-save]': 'onSave',
        'click button[on-cancel]': 'onCancel',
    },
    state: null,

    initialize() {
        this.template = ((template) => {
            return (params) => {
                return Mustache.render(template, params);
            };
        })(tpl)

        this._initState();
        this.render();

        this.listenTo(this.state, 'change', this.render);
        this.listenTo(this.model, 'destroy', this.remove);
    },

    render() {
        let data = this.model.toJSON();

        data.__state = this.state.toJSON();
        this.el.innerHTML = this.template(data);

        return this.el;
    },

    onEdit() {
        console.log('123');
        this.state.set({
            isEditable: true,
            name: this.model.get('name'),
            phone: this.model.get('phone')
        })
    },

    onDelete() {
        this.model.destroy();
    },

    onSave() {
        try {
            let name = this.el.querySelector('input[name="name"]').value,
                phone = this.el.querySelector('input[name="phone"]').value;

            this._updateState(name, phone, true);
            this._updateModel(name, phone, true);

            contactProvider
                .update({
                    name,
                    phone
                })
                .then(() => {
                    this._clearState();
                });

        } catch (e) {
            if (typeof e.getAttributeErrors !== 'function') {
                throw e;
            }
            this._processValidationException(e);
        }
    },

    onCancel() {
        this._clearState();
    },

    _updateModel(name, phone, silent) {
        this.model.set({
            name,
            phone
        }, {
            silent: !!silent,
            validate: true
        });
    },

    _updateState(name, phone, silent) {
        this.state.set({
            name,
            phone
        }, {
            silent: !!silent
        });
    },

    _clearState(silent) {
        this.state.set({
            isEditable: false,
            error: null,
            name: undefined,
            phone: undefined
        }, {
            silent: !!silent
        })
    },

    _initState() {
        this.state = new StateModel({
            isEditable: false,
            error: null,
            name: undefined,
            phone: undefined
        }, {
            silent: true
        });
    },

    _processValidationException(e) {
        let error = {},
            attributeErrors = e.getAttributeErrors();

        for (let prop in attributeErrors) {
            if (!attributeErrors.hasOwnProperty(prop)) {
                continue;
            }
            error[prop] = attributeErrors[prop].message;
        }

        this.state.set({error})
    }

});