import Backbone from "backbone";
import _ from "underscore";

import tpl from 'bundle-text:../../../templates/data/Phonebook/ContactTableView.html';

import contactCollection from "../../collections/ContactCollection";
import contactProvider from "../../services/ContactProvider";
import ContactTableItemView from "./ContactTableItemView";


export default Backbone.View.extend({
    events: {

    },

    selector: {
        tbody: '.phonebook-table tbody'
    },

    initialize() {
        this.template = _.template(tpl);

        this.render();

        this.collection = contactCollection;

        contactProvider.getAll().then((contacts) => {
            this.collection.add(contacts);
        })

        this.listenTo(this.collection, 'add', this.addOne);
    },

    render() {
        this.el.innerHTML = this.template();

        return this.el;
    },

    addOne(model) {
        let view = new ContactTableItemView({model: model});
        this.el.querySelector(this.selector.tbody).appendChild(view.render());
    }
});