

class ValidatorService {

    isEmpty(value) {
        return value === '';
    }

    isPhone(value) {
        return false === /^(8|\+?7)\s?\(?(\d){3,4}\)?\s?(\d){2,3}(\s|-)?(\d){2}(\s|-)?(\d){2}(\s|-)?$/.test(value)
    }
}

const validatorService = new ValidatorService();
export default validatorService;