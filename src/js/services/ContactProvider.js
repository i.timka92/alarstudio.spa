import dataProviderMock from "./mocks/DataProviderMock";


class ContactProvider {
    async getAll() {
        return dataProviderMock.getAllContacts();
    }

    async insert(data) {
        return dataProviderMock.updateContact(data);
    }

    async update(data) {
        return dataProviderMock.insertContact(data);
    }
}

const contactProvider = new ContactProvider();
export default contactProvider;