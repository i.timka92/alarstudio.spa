import timeoutHelper from "../../helpers/TimeoutHelper";


class DataProviderMock {
    async getAllContacts() {
        await timeoutHelper.delay(500);

        return [
            this.getOne('Иннокентий Смирнов', '89152632024'),
            this.getOne('Светлана Макарова', '89464467324'),
            this.getOne('Дарья Одинцова', '8987580357'),
            this.getOne('Руслан Самойлов', '89965813731'),
            this.getOne('Петр Зубцов', '89834801503'),
        ]
    }

    async insertContact(data) {
        await timeoutHelper.delay(500);

        return {
            data,
            status: 'ok',
            code: 200
        }
    }

    async updateContact(data) {
        await timeoutHelper.delay(500);

        return {
            data,
            status: 'ok',
            code: 200
        }
    }

    getOne(name, phone) {
        return {
            name,
            phone
        }
    }
}

const dataProviderMock = new DataProviderMock();
export default dataProviderMock;